/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perpus;

import perpus.databuku;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.File;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Asus
 */
public class datakembalian extends javax.swing.JFrame {
private Connection con;
private Statement stat;
private ResultSet res;
private String t;
private PreparedStatement pst;
JasperReport jasperReport1;
    JasperDesign jasperDesign1;
    JasperPrint jasperPrint1;
    Map<String, Object> param = new HashMap<String, Object>();


private void koneksi(){
try {
Class.forName("com.mysql.jdbc.Driver");
con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost/db_perpustakaan", "root", "");
stat=(Statement) con.createStatement();
} catch (ClassNotFoundException | SQLException e) {
JOptionPane.showMessageDialog(null, e);
}
}
private void kosongkan()
{ 
kode_pinjam.setText("");  
kode_pinjam.setText(""); 
no_anggota.setText("");
nama.setText("");
kode_buku.setText("");
judul.setText("");
penerbit.setText("");
tgl_kembali.setText("");
tgl_pinjam.setText("");
jumlah.setEnabled(false);
sisa.setEnabled(false);
jumlah.setText("0");
sisa.setText("0");
kode_pinjam.requestFocus();


 } 

 java.util.Date tglsekarang = new java.util.Date();
    private SimpleDateFormat smpdtfmt = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    //diatas adalah pengaturan format penulisan, bisa diubah sesuai keinginan.
      private SimpleDateFormat as = new SimpleDateFormat("yyyy-MM-dd");
    private String tanggal = as.format(tglsekarang);
    private Statement stm;
     
    /**
     * Creates new form Pembeli
     */
    public datakembalian() {
        initComponents();
        koneksi();
        kosongkan();
          id_auto();
         tgl.setText(tanggal);
         
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = getSize();
        setLocation(
        (screenSize.width - frameSize.width) / 2,
        (screenSize.height - frameSize.height) / 2);
    }
private void id_auto(){
    try{
            java.sql.Connection conn = (Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            String sql = "select max(right(kode_kembali,4)) as no from pengembalian";
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()){
                if (res.first() == false){
                   kode_kembali.setText("KEM001");
                }else{
                       res.last();
                       int set_id = res.getInt(1)+1;
                       String no = String.valueOf(set_id);
                       int id_next = no.length();
                       for (int a = 0 ; a <4 - id_next; a++){
                           no = "0" + no;
                       }
                       kode_kembali.setText("KEM" + no);
                }
                
            }
    } catch (SQLException ex) {
      //  Logger.getLogger(datapeminjaman.class.getName()).log(Level.SEVERE, null, ex);
    }
}
/*
private void status(){
           try{
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            String sql = "select status from peminjaman where kode_pinjam ='%"+kode_pinjam.getText()+"%'";
            java.sql.ResultSet res = stm.executeQuery(sql);
            while(res.next()){
                String name = res.getString("status");
              //  String nam  = res.getString("no_kelas");
                status.setSelectedItem(name);
               // kd_kelas.addItem(nam);
            }
           }catch(Exception e){
           }
    }*/
private void pinjam(){
           try{
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            String sql = "select * from peminjaman where kode_pinjam like'%"+kode_pinjam.getText()+"%'";
           java.sql.ResultSet res = stm.executeQuery(sql);
            while(res.next()){
               
                 kode_pinjam.setText(res.getString("kode_pinjam")); 
                 no_anggota.setText(res.getString("no_anggota"));
                 nama.setText(res.getString("nama"));
                 
                 tgl_pinjam.setText(res.getString("tanggal_pinjam")); 
                 tgl_kembali.setText(res.getString("tanggalharuskembali"));
                    kode_buku.setText(res.getString("kdbuku")); 
                 judul.setText(res.getString("judul"));
                 penerbit.setText(res.getString("penerbit"));
                 jumlah.setText(res.getString("jumlah"));
       
            }
           }catch(Exception e){
           }
    }
   
 





    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        peminjam = new javax.swing.JDialog();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_pinjam = new javax.swing.JTable();
        cari1 = new java.awt.TextField();
        jLabel1 = new javax.swing.JLabel();
        label2 = new java.awt.Label();
        jPanel1 = new javax.swing.JPanel();
        kode_pinjam = new javax.swing.JTextField();
        nama = new javax.swing.JTextField();
        kode_buku = new javax.swing.JTextField();
        btedit = new javax.swing.JButton();
        bthapus = new javax.swing.JButton();
        btsimpan = new javax.swing.JButton();
        btkembali = new javax.swing.JButton();
        no_anggota = new javax.swing.JTextField();
        judul = new javax.swing.JTextField();
        penerbit = new javax.swing.JTextField();
        tgl_pinjam = new javax.swing.JTextField();
        kode_kembali = new javax.swing.JTextField();
        tgl_kembali = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl = new javax.swing.JTable();
        keterlambatan = new javax.swing.JTextField();
        denda = new javax.swing.JTextField();
        cari = new javax.swing.JTextField();
        jumlah = new java.awt.TextField();
        tgl = new java.awt.Label();
        sisa = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jLabel4 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        tbl_pinjam.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_pinjam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_pinjamMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_pinjam);

        cari1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cari1KeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setText("Cari");

        label2.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        label2.setText("DATA PEMINJAMAN");

        javax.swing.GroupLayout peminjamLayout = new javax.swing.GroupLayout(peminjam.getContentPane());
        peminjam.getContentPane().setLayout(peminjamLayout);
        peminjamLayout.setHorizontalGroup(
            peminjamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, peminjamLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(287, 287, 287))
            .addGroup(peminjamLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(cari1, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
            .addGroup(peminjamLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        peminjamLayout.setVerticalGroup(
            peminjamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(peminjamLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(peminjamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cari1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 204, 204));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setForeground(new java.awt.Color(0, 20, 0));
        jPanel1.setMaximumSize(new java.awt.Dimension(1000, 667));
        jPanel1.setMinimumSize(new java.awt.Dimension(1000, 667));
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 667));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        kode_pinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kode_pinjamActionPerformed(evt);
            }
        });
        jPanel1.add(kode_pinjam, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 140, 150, 30));

        nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaActionPerformed(evt);
            }
        });
        jPanel1.add(nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 240, 190, 30));

        kode_buku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kode_bukuActionPerformed(evt);
            }
        });
        jPanel1.add(kode_buku, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 140, 190, 30));

        btedit.setContentAreaFilled(false);
        btedit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btedit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btedit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bteditActionPerformed(evt);
            }
        });
        jPanel1.add(btedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 400, 110, 40));

        bthapus.setContentAreaFilled(false);
        bthapus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bthapus.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bthapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bthapusActionPerformed(evt);
            }
        });
        jPanel1.add(bthapus, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 400, 110, 40));

        btsimpan.setContentAreaFilled(false);
        btsimpan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btsimpan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btsimpanActionPerformed(evt);
            }
        });
        jPanel1.add(btsimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 400, 120, 40));

        btkembali.setContentAreaFilled(false);
        btkembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btkembaliActionPerformed(evt);
            }
        });
        jPanel1.add(btkembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 400, 130, 40));
        jPanel1.add(no_anggota, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 190, 190, 30));
        jPanel1.add(judul, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 190, 190, 30));
        jPanel1.add(penerbit, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 240, 190, 30));

        tgl_pinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tgl_pinjamActionPerformed(evt);
            }
        });
        jPanel1.add(tgl_pinjam, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 290, 80, 30));

        kode_kembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kode_kembaliActionPerformed(evt);
            }
        });
        jPanel1.add(kode_kembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 90, 130, 30));
        jPanel1.add(tgl_kembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 290, 80, 30));
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 170, -1, -1));

        tbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 980, 90));

        keterlambatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keterlambatanActionPerformed(evt);
            }
        });
        keterlambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                keterlambatanKeyReleased(evt);
            }
        });
        jPanel1.add(keterlambatan, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 350, 140, 30));

        denda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dendaActionPerformed(evt);
            }
        });
        jPanel1.add(denda, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 350, 150, 30));

        cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cariKeyReleased(evt);
            }
        });
        jPanel1.add(cari, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 400, 30));
        jPanel1.add(jumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 290, 150, 30));

        tgl.setBackground(new java.awt.Color(255, 255, 255));
        tgl.setText("label2");
        jPanel1.add(tgl, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 50, -1, -1));

        sisa.setEditable(false);
        jPanel1.add(sisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 290, 40, 30));

        jButton1.setText("...");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 140, 40, 30));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("-");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 300, 30, 20));

        jToggleButton1.setContentAreaFilled(false);
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jToggleButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 400, 120, 50));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/pengembalian.jpg"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kode_pinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kode_pinjamActionPerformed

        buku();
        pinjam();
//         status(); 
         tambahbuku();
         //hari();
        hari1();
//         denda();
    }//GEN-LAST:event_kode_pinjamActionPerformed

    private void namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaActionPerformed

    private void kode_bukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kode_bukuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kode_bukuActionPerformed

    private void btsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btsimpanActionPerformed
         
        try{
       String sql = "update databuku set jumlah='" + sisa.getText() + "' where kdbuku='"+kode_buku.getText()+"'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.PreparedStatement pst = conn.prepareStatement(sql);
            pst.execute();
    }
    catch (SQLException ex) {
        Logger.getLogger(datapeminjaman.class.getName()).log(Level.SEVERE, null, ex);
    } 
        try{
       String sql = "delete from peminjaman where kode_pinjam ='"+kode_pinjam.getText()+"'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.PreparedStatement pst = conn.prepareStatement(sql);
            pst.execute();
    }
    catch (SQLException ex) {
        Logger.getLogger(datapeminjaman.class.getName()).log(Level.SEVERE, null, ex);
    } 
        
             try {
 stat.executeUpdate("insert into  pengembalian values (" 
+ "'" + kode_kembali.getText()+"',"
+ "'" + kode_pinjam.getText()+"',"
+ "'" + no_anggota.getText()+"',"
+ "'" + nama.getText()+"',"
+ "'" + tgl_pinjam.getText()+"',"
+ "'" + tgl_kembali.getText()+ "',"
+ "'" + kode_buku.getText()+"',"
+ "'" + judul.getText()+"',"
+ "'" + penerbit.getText()+"',"
+ "'" + jumlah.getText()+"',"
+ "'" + keterlambatan.getText()+"',"
+ "'" + denda.getText()+"'" + ")");
kosongkan(); 
JOptionPane.showMessageDialog(null, "Berhasil Menyimpan Data"); 

} catch (SQLException | HeadlessException e) { 
JOptionPane.showMessageDialog(null, "Perintah Salah : "+e);
 }finally{
//         tabel();
           id_auto();
           tambahbuku();
             }
    }//GEN-LAST:event_btsimpanActionPerformed

    private void btkembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btkembaliActionPerformed
        // TODO add your handling code here:
        new menu_utama().setVisible(true);
        dispose();
    }//GEN-LAST:event_btkembaliActionPerformed

    private void bteditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bteditActionPerformed
        // TODO add your handling code here:
        try {
            stat.executeUpdate("update pengembalian   set "
               + "kode_kembali='"+kode_kembali.getText()+"',"
                + "kode_pinjam='"+kode_pinjam.getText()+"',"
                + "no_anggota='"+no_anggota.getText()+"',"
                + "nama='"+nama.getText()+"',"
                + "kdbuku='"+kode_buku.getText()+"',"
                + "judul='"+judul.getText()+"',"
                + "penerbit='"+penerbit.getText()+"',"
                + "jumlah='"+jumlah.getText()+"',"
                + "tanggal_pinjam='"+tgl_pinjam.getText()+"'"
                 + "tanggal_kembali='"+tgl_kembali.getText()+"'"   
                 
                + " where " + "kode_kembali='"+kode_kembali.getText()+"'" );
            kosongkan();
            JOptionPane.showMessageDialog(rootPane, "Data berhasil Di update");
        } catch (SQLException | HeadlessException e) {
            JOptionPane.showMessageDialog(rootPane, e);
        }finally{
         //tabel();
           id_auto();
        }
    }//GEN-LAST:event_bteditActionPerformed

    private void bthapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bthapusActionPerformed
        // TODO add your handling code here:
          try { 
stat.executeUpdate("delete from pengembalian where " 
+ "kode_kembali='"+kode_kembali.getText()
+"'" ); 
kosongkan(); 
JOptionPane.showMessageDialog(null, "Berhasil");
 } catch (SQLException | HeadlessException e) { 
JOptionPane.showMessageDialog(null, "pesan salah : "+e);
 } finally{
         //tabel();
           id_auto();
          }
    }//GEN-LAST:event_bthapusActionPerformed

    private void tblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMouseClicked
int baris;
           baris = tbl.getSelectedRow();
      
     //   jdate.SetDate(tabel_diagnosa.getValueAt(baris,0).toString());
         kode_kembali.setText(tbl.getValueAt(baris,0).toString());
        kode_pinjam.setText(tbl.getValueAt(baris,1).toString());
        no_anggota.setText(tbl.getValueAt(baris,2).toString());
        nama.setText(tbl.getValueAt(baris,3).toString());
        kode_buku.setText(tbl.getValueAt(baris,4).toString());
        judul.setText(tbl.getValueAt(baris,5).toString());
        penerbit.setText(tbl.getValueAt(baris,6).toString());
        jumlah.setText(tbl.getValueAt(baris,7).toString());
        tgl_pinjam.setText(tbl.getValueAt(baris,8).toString());
        tgl_kembali.setText(tbl.getValueAt(baris,9).toString());
        keterlambatan.setText(tbl.getValueAt(baris,10).toString());
        denda.setText(tbl.getValueAt(baris,11).toString());
        // TODO add your handling code here:
    }//GEN-LAST:event_tblMouseClicked

    private void keterlambatanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keterlambatanKeyReleased
       
// int jumlahbaris = info_pengembalian.getRowCount();
        /*
        int total = 0;
        
        int telat;
        int sewa=1000;
        
        telat = Integer.valueOf(keterlambatan.getText());
        if(telat>1){
        total=telat*1000;
        denda.setText(String.valueOf(total));
        }else if(telat<=0){
        total=telat*0;
        }
        */
    }//GEN-LAST:event_keterlambatanKeyReleased

    private void dendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dendaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dendaActionPerformed

    private void keterlambatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keterlambatanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_keterlambatanActionPerformed

    private void kode_kembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kode_kembaliActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kode_kembaliActionPerformed

    private void tgl_pinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tgl_pinjamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tgl_pinjamActionPerformed

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
peminjam.pack();
peminjam.setVisible(true);
peminjam.setAlwaysOnTop(true);
dialog();
    }//GEN-LAST:event_jButton1MouseClicked

    private void cari1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cari1KeyReleased
dialog();        // TODO add your handling code here:
    }//GEN-LAST:event_cari1KeyReleased

    private void tbl_pinjamMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_pinjamMouseClicked
   
        int baris;
           baris = tbl_pinjam.getSelectedRow();
         kode_pinjam.setText(tbl_pinjam.getValueAt(baris,1).toString());
         no_anggota.setText(tbl_pinjam.getValueAt(baris,2).toString());
         nama.setText(tbl_pinjam.getValueAt(baris,3).toString());
         tgl_pinjam.setText(tbl_pinjam.getValueAt(baris,8).toString());
         tgl_kembali.setText(tbl_pinjam.getValueAt(baris,9).toString());
         kode_buku.setText(tbl_pinjam.getValueAt(baris,4).toString());
         judul.setText(tbl_pinjam.getValueAt(baris,5).toString());
         penerbit.setText(tbl_pinjam.getValueAt(baris,6).toString());
         jumlah.setText(tbl_pinjam.getValueAt(baris,7).toString());
          
       peminjam.setVisible(false);
          buku();
        pinjam();
           hari1();
        //coba();
         tambahbuku();
        denda();
       
    }//GEN-LAST:event_tbl_pinjamMouseClicked

    private void cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cariKeyReleased
tampil();        // TODO add your handling code here:
    }//GEN-LAST:event_cariKeyReleased

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
reload();        // TODO add your handling code here:
    }//GEN-LAST:event_jToggleButton1ActionPerformed

       
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(datakembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(datakembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(datakembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(datakembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
       
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new datakembalian().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btedit;
    private javax.swing.JButton bthapus;
    private javax.swing.JButton btkembali;
    private javax.swing.JButton btsimpan;
    private javax.swing.JTextField cari;
    private java.awt.TextField cari1;
    private javax.swing.JTextField denda;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JTextField judul;
    private java.awt.TextField jumlah;
    private javax.swing.JTextField keterlambatan;
    private javax.swing.JTextField kode_buku;
    private javax.swing.JTextField kode_kembali;
    private javax.swing.JTextField kode_pinjam;
    private java.awt.Label label2;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField no_anggota;
    private javax.swing.JDialog peminjam;
    private javax.swing.JTextField penerbit;
    private javax.swing.JTextField sisa;
    private javax.swing.JTable tbl;
    private javax.swing.JTable tbl_pinjam;
    private java.awt.Label tgl;
    private javax.swing.JTextField tgl_kembali;
    private javax.swing.JTextField tgl_pinjam;
    // End of variables declaration//GEN-END:variables
private void kurangtanggal(){
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
   
}

private void tambahbuku(){
int sisa1, qty;
   int kembali = 0;

        qty = Integer.valueOf(jumlah.getText());
        sisa1 = Integer.valueOf(sisa.getText());
        kembali = qty+sisa1;
        sisa.setText(String.valueOf(kembali));  

}

private void hari(){
Integer jml, qty;
   Integer kembali ;

        qty = Integer.valueOf(tgl_kembali.getText().substring(8, 10));
        jml = Integer.valueOf(tgl.getText().substring(8, 10));
        kembali = qty-jml;
        keterlambatan.setText(String.valueOf(kembali));  

}

 private void buku(){
           try{
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            

              //String sql = "SELECT nama_kelas FROM data_kelas WHERE kd_kelas IN (SELECT kd_kelas FROM data_kelas)";
                String sql = "SELECT * from databuku where kdbuku='"+kode_buku.getText()+"'";
            java.sql.ResultSet res = stm.executeQuery(sql);
            while(res.next()){
                String name = res.getString("jumlah");
               // String nam  = res.getString("no_kelas");
                sisa.setText(name);
                //kd_kelas1.addItem(nam);
            }
    
           }catch(Exception e){
           }
    }

 /*
 private void denda(){
  int ab = Integer.valueOf(keterlambatan.getText());
        int den = ab*1000;
        denda.setText(Integer.toString(den));
 }*/
 private void cetak(){
         //if (nisn.getText()) {
         String pilihan[] = {"Ya ", "Tidak "};
        int pesan = JOptionPane.showOptionDialog(this, "cetak bukti transaksi ?","Cetak",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, pilihan, pilihan[0]);
        if(pesan == JOptionPane.YES_OPTION){
             try{

            File file = new File("src/laporan/kwitansi_pembayaran_siswa.jrxml");
            jasperDesign1 = JRXmlLoader.load(file);
            jasperReport1 = JasperCompileManager.compileReport(jasperDesign1);
            param.put("kd_pinjam", kode_pinjam.getText().toString());
            jasperPrint1 = JasperFillManager.fillReport(jasperReport1, param,method.koneksi.getKoneksi());

            JasperViewer.viewReport(jasperPrint1,false);
        } catch (Exception e) {
        }
        }
    }
 
 private void hari1(){
     int tk, ti;
     int h=0;
     int m=0;
     
     //String n="";

     tk=Integer.parseInt(tgl_kembali.getText().substring(8, 10));
     ti=Integer.parseInt(tgl.getText().substring(8, 10));
     
     h=ti-tk;
   
    keterlambatan.setText(String.valueOf(h));
  
}
 
 private void dialog(){ 
DefaultTableModel t= new DefaultTableModel();
 t.addColumn("NO");
 t.addColumn("Kode Pinjam"); 
 t.addColumn("No anggota "); 

 t.addColumn("Nama Anggota");

 t.addColumn("kode buku");
 t.addColumn("Judul");
 t.addColumn("Penerbit");
 t.addColumn("jumlah");
 t.addColumn("Tanggal Pinjam"); 
 t.addColumn("Tgl Harus Kembali");
 try {
            int no = 1;
            String sql = "select * from peminjaman where kode_pinjam like '%"+cari1.getText()+"%' and no_anggota like '%"+cari1.getText()+"%'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                t.addRow(new Object[]{no++,res.getString(1), res.getString(2), res.getString(4), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10), res.getString(11)});
            }
            tbl_pinjam.setModel(t);
        } catch (Exception e) {
        
        }
 
}
 private void denda(){
 int total = 0;
        String a="0";
        int telat;
        int sewa=1000;
        
        telat = Integer.valueOf(keterlambatan.getText());
        if(telat>=1){
        total=telat*1000;
        denda.setText(String.valueOf(total));
        }else if(telat<=0){
        total=0;
        denda.setText(String.valueOf(total));
        }
 }
 
 private void coba(){
   int lambat, dibayar;
   int kembali = 0;

        lambat = Integer.valueOf(keterlambatan.getText());
        dibayar = 1000;
        kembali = lambat*dibayar;
        denda.setText(String.valueOf(kembali));
        if(lambat <=0)
        {
            dibayar = 1000;
            kembali = lambat*dibayar;
           denda.setText(String.valueOf(kembali));
        }else if(lambat>=1){
            dibayar = 0;
            kembali = lambat*dibayar;
           denda.setText(String.valueOf(kembali));
        }
 
 }
 
 private void reload(){
     new datakembalian().setVisible(true);
     this.dispose();
   }


private void tampil(){ 
DefaultTableModel t= new DefaultTableModel();
 t.addColumn("NO");
 t.addColumn("Kode Pengembalian"); 
 t.addColumn("Kode Peminjaman"); 
 t.addColumn("No Anggota"); 
 t.addColumn("Nama Anggota");
 t.addColumn("Tanggal Pinjam");
 t.addColumn("Tanggal Harus Kembali");
 t.addColumn("Kode Buku");
 t.addColumn("Judul Buku");
 t.addColumn("Penerbit"); 
 t.addColumn("Jumlah Pinjam");
 t.addColumn("Keterlambatan");
 t.addColumn("Total Denda");
 try {
            int no = 1;
            String sql = "select * from pengembalian where nama like '%"+cari.getText()+"%' and no_anggota like '%"+cari.getText()+"%'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                t.addRow(new Object[]{no++,res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10), res.getString(11), res.getString(12)});
            }
            tbl.setModel(t);
        } catch (Exception e) {
        
        }
 
}

}




