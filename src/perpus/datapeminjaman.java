/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perpus;


import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Asus
 */
public class datapeminjaman extends javax.swing.JFrame {
private Connection con;
private Statement stat;
private ResultSet res;
private String t;
private PreparedStatement pst;


private void koneksi(){
try {
Class.forName("com.mysql.jdbc.Driver");
con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost/db_perpustakaan", "root", "");
stat=(Statement) con.createStatement();
} catch (ClassNotFoundException | SQLException e) {
JOptionPane.showMessageDialog(null, e);
}
}
private void kosongkan()
{
   
    kosong();
kode_pinjam.setText(""); 
no_anggota.setText("");
nis.setText("");
alamat.setText("");
tanggalharuskembali.setDate(null);
nama.setText("");
kode_buku.setText("");
judul.setText("");
penerbit.setText("");
jumlah.setText("");

jml_pinjam.setText("");
kode_pinjam.requestFocus();


 } 
private void bersih()
{ 
kode_buku.setText(""); 
judul.setText("");
penerbit.setText("");
nis.setText("");
alamat.setText("");
}
 java.util.Date tglsekarang = new java.util.Date();
    private SimpleDateFormat smpdtfmt = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    //diatas adalah pengaturan format penulisan, bisa diubah sesuai keinginan.
    private String tanggal = smpdtfmt.format(tglsekarang);
    private Statement stm;
    private SimpleDateFormat as = new SimpleDateFormat("yyyy-MM-dd");
    
    /**
     * Creates new form Pembeli
     */
    public datapeminjaman() {
         
        initComponents();
        koneksi();
        kosongkan();
        bersih();
        tabel();
  
        id_auto();
        Locale.setDefault(new Locale("in", "ID"));
//        tabel2();
//        tabel3();
     
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = getSize();
        setLocation(
        (screenSize.width - frameSize.width) / 2,
        (screenSize.height - frameSize.height) / 2);
    }

private void id_auto(){
    try{
            java.sql.Connection conn = (Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            String sql = "select max(right(kode_pinjam,4)) as no from peminjaman";
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()){
                if (res.first() == false){
                   kode_pinjam.setText("TR001");
                }else{
                       res.last();
                       int set_id = res.getInt(1)+1;
                       String no = String.valueOf(set_id);
                       int id_next = no.length();
                       for (int a = 0 ; a <4 - id_next; a++){
                           no = "0" + no;
                       }
                       kode_pinjam.setText("TR" + no);
                }
                
            }
    } catch (SQLException ex) {
      //  Logger.getLogger(datapeminjaman.class.getName()).log(Level.SEVERE, null, ex);
    }
}



private void load_pinjam() {
     
      
  DefaultTableModel t= new DefaultTableModel();
 t.addColumn("No"); 
 t.addColumn("Id pinjam"); 
 t.addColumn("no_anggota");
 t.addColumn("Kode Buku"); 
 t.addColumn("Judul");
 t.addColumn("penerbit");
 t.addColumn("jumlah");
        
         try {
            int no = 1;
            String sql = "select * from peminjaman where no_anggota like '%" + no_anggota.getText() + "%' and jumlah >=1";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                t.addRow(new Object[]{no++, res.getString(1), res.getString(2), res.getString(6), res.getString(7), res.getString(8), res.getString(9)});
            }
           tb_buku.setModel(t);
        } catch (Exception e) {
        }
}



private void tabel(){ 
DefaultTableModel t= new DefaultTableModel();
 t.addColumn("NO");
 t.addColumn("Kode Pinjam"); 
 t.addColumn("No anggota "); 
 t.addColumn("Nis"); 
 t.addColumn("Nama Anggota");
 t.addColumn("Alamat");
 t.addColumn("kode buku");
 t.addColumn("Judul");
 t.addColumn("Penerbit");
 t.addColumn("jumlah");
 t.addColumn("Tanggal Pinjam"); 
 t.addColumn("Tgl Harus Kembali");
 try {
            int no = 1;
            String sql = "select * from peminjaman where kode_pinjam like '%"+kode_pinjam.getText()+"%' and no_anggota like '%"+no_anggota.getText()+"%'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                t.addRow(new Object[]{no++,res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10), res.getString(11)});
            }
            tbl.setModel(t);
        } catch (Exception e) {
        
        }
 
}


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        peminjamanDialog = new javax.swing.JDialog();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_buku = new javax.swing.JTable();
        cari = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        kode_pinjam = new javax.swing.JTextField();
        tanggalharuskembali = new com.toedter.calendar.JDateChooser();
        nama = new javax.swing.JTextField();
        kode_buku = new javax.swing.JTextField();
        btedit = new javax.swing.JButton();
        bthapus = new javax.swing.JButton();
        btsimpan = new javax.swing.JButton();
        btkembali = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl = new javax.swing.JTable();
        no_anggota = new javax.swing.JTextField();
        judul = new javax.swing.JTextField();
        penerbit = new javax.swing.JTextField();
        txtcari3 = new javax.swing.JTextField();
        cari_peminjaman = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tb_buku = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        nis = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        alamat = new javax.swing.JTextArea();
        jml_pinjam = new javax.swing.JTextField();
        jumlah = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        btncaribuku = new javax.swing.JButton();
        jToggleButton1 = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        tbl_buku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_buku.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_bukuMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_buku);

        jLabel2.setText("Cari");

        javax.swing.GroupLayout peminjamanDialogLayout = new javax.swing.GroupLayout(peminjamanDialog.getContentPane());
        peminjamanDialog.getContentPane().setLayout(peminjamanDialogLayout);
        peminjamanDialogLayout.setHorizontalGroup(
            peminjamanDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, peminjamanDialogLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(peminjamanDialogLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        peminjamanDialogLayout.setVerticalGroup(
            peminjamanDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, peminjamanDialogLayout.createSequentialGroup()
                .addContainerGap(76, Short.MAX_VALUE)
                .addGroup(peminjamanDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(29, 29, 29)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 204, 204));
        setMaximumSize(new java.awt.Dimension(1000, 667));
        setMinimumSize(new java.awt.Dimension(1000, 667));
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setForeground(new java.awt.Color(0, 20, 0));
        jPanel1.setMaximumSize(new java.awt.Dimension(1000, 667));
        jPanel1.setMinimumSize(new java.awt.Dimension(1000, 667));
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 667));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        kode_pinjam.setEditable(false);
        kode_pinjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kode_pinjamActionPerformed(evt);
            }
        });
        jPanel1.add(kode_pinjam, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 90, 120, 30));
        jPanel1.add(tanggalharuskembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 140, 120, 30));

        nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaActionPerformed(evt);
            }
        });
        jPanel1.add(nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 230, 190, 30));

        kode_buku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kode_bukuActionPerformed(evt);
            }
        });
        jPanel1.add(kode_buku, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 200, 170, 30));

        btedit.setContentAreaFilled(false);
        btedit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btedit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btedit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bteditActionPerformed(evt);
            }
        });
        jPanel1.add(btedit, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 400, 110, 40));

        bthapus.setContentAreaFilled(false);
        bthapus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bthapus.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bthapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bthapusActionPerformed(evt);
            }
        });
        jPanel1.add(bthapus, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 400, 120, 40));

        btsimpan.setContentAreaFilled(false);
        btsimpan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btsimpan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btsimpanActionPerformed(evt);
            }
        });
        jPanel1.add(btsimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 400, 120, 50));

        btkembali.setContentAreaFilled(false);
        btkembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btkembaliActionPerformed(evt);
            }
        });
        jPanel1.add(btkembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 400, 130, 50));

        tbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9"
            }
        ));
        tbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 510, 580, 130));

        no_anggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                no_anggotaActionPerformed(evt);
            }
        });
        jPanel1.add(no_anggota, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, 190, 30));
        jPanel1.add(judul, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 240, 170, 30));
        jPanel1.add(penerbit, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 290, 170, 30));

        txtcari3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcari3ActionPerformed(evt);
            }
        });
        txtcari3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcari3KeyReleased(evt);
            }
        });
        jPanel1.add(txtcari3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, 350, 30));

        cari_peminjaman.setContentAreaFilled(false);
        cari_peminjaman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cari_peminjamanActionPerformed(evt);
            }
        });
        jPanel1.add(cari_peminjaman, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 463, 130, 30));

        tb_buku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tb_buku.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tb_bukuMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tb_buku);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 510, 400, 130));

        jButton1.setContentAreaFilled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 400, 120, 50));

        nis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nisActionPerformed(evt);
            }
        });
        jPanel1.add(nis, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 190, 190, 30));

        alamat.setColumns(20);
        alamat.setRows(5);
        jScrollPane4.setViewportView(alamat);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 290, 190, 80));

        jml_pinjam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jml_pinjamKeyReleased(evt);
            }
        });
        jPanel1.add(jml_pinjam, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 330, 170, 30));

        jumlah.setEditable(false);
        jumlah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jumlahActionPerformed(evt);
            }
        });
        jPanel1.add(jumlah, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 290, 40, 30));

        jButton2.setText("...");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 200, 50, 30));

        btncaribuku.setContentAreaFilled(false);
        btncaribuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncaribukuActionPerformed(evt);
            }
        });
        jPanel1.add(btncaribuku, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 460, 100, 30));

        jToggleButton1.setContentAreaFilled(false);
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jToggleButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 400, 120, 40));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/peminjaman.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1060, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(1000, 689));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void kode_pinjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kode_pinjamActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kode_pinjamActionPerformed

    private void namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaActionPerformed

    private void kode_bukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kode_bukuActionPerformed
        try { 
res=stat.executeQuery("select * from databuku where "+ "kdbuku='" +kode_buku.getText()
+"'" ); while (res.next()) 
{   kode_buku.setText(res.getString("kdbuku")); 
    judul.setText(res.getString("judul"));
    penerbit.setText(res.getString("penerbit"));
 

}                                        
 }       
catch (Exception e) { 
JOptionPane.showMessageDialog(rootPane, e); 
}
        // TODO add your handling code here:
    }//GEN-LAST:event_kode_bukuActionPerformed

    private void btsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btsimpanActionPerformed
        // TODO add your handling code here:
        try{
       String sql = "update databuku set jumlah='" + jumlah.getText() + "' where kdbuku='"+kode_buku.getText()+"'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.PreparedStatement pst = conn.prepareStatement(sql);
            pst.execute();
    }
    catch (SQLException ex) {
        Logger.getLogger(datapeminjaman.class.getName()).log(Level.SEVERE, null, ex);
    } 
  try {

            String sql;
        sql = "INSERT INTO peminjaman VALUES ('" + kode_pinjam.getText()+"',"
                + "'" + no_anggota.getText()+"',"
                + "'" + nis.getText()+"',"
                + "'" + nama.getText()+"',"
                + "'" + alamat.getText()+"',"
                + "'" + kode_buku.getText()+"',"
                + "'" + judul.getText()+"',"
                + "'" + penerbit.getText()+"'," 
                + "'" +jml_pinjam.getText()+"',"
//                + "'" + as.format(tgl_pinjam.getDate())+"',"
                + "'" + as.format(tanggalharuskembali.getDate())+ "')";
     
            kosongkan();
             java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.PreparedStatement pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Penyimpanan Data Transaksi Berhasil");
        } catch (SQLException | HeadlessException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
    }
     finally{
            tabel();
            id_auto();
        
            load_pinjam();
            
        }
        
        
    }//GEN-LAST:event_btsimpanActionPerformed

    private void btkembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btkembaliActionPerformed
        // TODO add your handling code here:
        new menu_utama().setVisible(true);
        dispose();
    }//GEN-LAST:event_btkembaliActionPerformed

    private void bteditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bteditActionPerformed
       // TODO add your handling code here:
      try {
//            String sql = "update peminjaman set no_anggota='" + no_anggota.getText()+ "',nis='"+nis.getText()+ "',nama='"+nama.getText()+ "',alamat='"+alamat.getText()+ "',kdbuku='"+kode_buku.getText()+ "',judul='"+judul.getText()+ "',penerbit='"+penerbit.getText()+ "',jumlah='"+jumlah.getText()+ "',tanggal_pinjam='"+as.format(tgl_pinjam.getDate())+ "',tanggalharuskembali='"+tanggalharuskembali.getDate()+ "' where kode_pinjam='"+kode_pinjam.getText()+"' and no_anggota='"+no_anggota.getText()+"'";
            String sql = "update peminjaman set no_anggota='" + no_anggota.getText()+ "',nis='"+nis.getText()+ "',nama='"+nama.getText()+ "',alamat='"+alamat.getText()+ "',kdbuku='"+kode_buku.getText()+ "',judul='"+judul.getText()+ "',penerbit='"+penerbit.getText()+ "',jumlah='"+jumlah.getText()+ "',tanggalharuskembali='"+tanggalharuskembali.getDate()+ "' where kode_pinjam='"+kode_pinjam.getText()+"' and no_anggota='"+no_anggota.getText()+"'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.PreparedStatement pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Data berhasil disimpan");
            //cetak();
        } catch (SQLException | HeadlessException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
      id_auto();
      load_pinjam();
      tabel();
    }//GEN-LAST:event_bteditActionPerformed

    private void bthapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bthapusActionPerformed
        // TODO add your handling code here:
          try { 
stat.executeUpdate("delete from peminjaman where "+ "kdbuku='" +kode_buku.getText()
+"'" );
kosongkan(); 
JOptionPane.showMessageDialog(null, "Berhasil");
 } catch (SQLException | HeadlessException e) { 
JOptionPane.showMessageDialog(null, "pesan salah : "+e);
 } finally{
         tabel();
         bersih();
         id_auto();
         load_pinjam();
          }
    }//GEN-LAST:event_bthapusActionPerformed

    private void cari_peminjamanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cari_peminjamanActionPerformed
      
        // TODO add your handling code here:
    }//GEN-LAST:event_cari_peminjamanActionPerformed

    private void tblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMouseClicked
 int baris;
           baris = tbl.getSelectedRow();
      
  //      jdate.SetDate(tabel_diagnosa.getValueAt(baris,0).toString());
        kode_pinjam.setText(tbl.getValueAt(baris,1).toString());
        
        no_anggota.setText(tbl.getValueAt(baris,2).toString());   
        
        nis.setText(tbl.getValueAt(baris,3).toString());   // TODO add your handling code here:
        
        nama.setText(tbl.getValueAt(baris,4).toString());
        
        alamat.setText(tbl.getValueAt(baris,5).toString());   
        
        kode_buku.setText(tbl.getValueAt(baris,6).toString());
        
        judul.setText(tbl.getValueAt(baris,7).toString());
        
        penerbit.setText(tbl.getValueAt(baris,8).toString());
        
        jumlah.setText(tbl.getValueAt(baris,9).toString());
        
        String l=(String)tbl.getModel().getValueAt(baris, 10);
try{
    SimpleDateFormat fa= new SimpleDateFormat("yyyy-MM-dd");
    java.util.Date da=fa.parse(l);
//    tgl_pinjam.setDate(da);
}catch(Exception ex){
    ex.printStackTrace();
}
        
        
        String s=(String)tbl.getModel().getValueAt(baris, 11);
try{
    SimpleDateFormat f= new SimpleDateFormat("yyyy-MM-dd");
    java.util.Date d=f.parse(s);
    tanggalharuskembali.setDate(d);
}catch(Exception ex){
    ex.printStackTrace();
}
        load_pinjam();
    }//GEN-LAST:event_tblMouseClicked

    private void no_anggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_no_anggotaActionPerformed
load_pinjam();       
        try { 
res=stat.executeQuery("select * from data_anggota where "+ "no_anggota='" +no_anggota.getText()
+"'" ); while (res.next()) 
{   no_anggota.setText(res.getString("no_anggota")); 
    nama.setText(res.getString("nama"));
    nis.setText(res.getString("nis"));
    alamat.setText(res.getString("alamat"));

}                                        
 }       
catch (Exception e) { 
JOptionPane.showMessageDialog(rootPane, e); 
}
        
        load_pinjam();
    
        // TODO add your handling code here:
    }//GEN-LAST:event_no_anggotaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        edit();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tb_bukuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tb_bukuMouseClicked
        // TODO add your handling code here:
        int baris;
           baris = tb_buku.getSelectedRow();
      
  //      jdate.SetDate(tabel_diagnosa.getValueAt(baris,0).toString());
        kode_buku.setText(tb_buku.getValueAt(baris,0).toString());
        judul.setText(tb_buku.getValueAt(baris,1).toString());        // TODO add your handling code here:
       
        judul.setText(tb_buku.getValueAt(baris,2).toString());
        penerbit.setText(tb_buku.getValueAt(baris,4).toString());
        //tgl_fktur.setText(tb_buku.getValueAt(baris,4).toString());
    }//GEN-LAST:event_tb_bukuMouseClicked

    private void nisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nisActionPerformed

    private void txtcari3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcari3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcari3ActionPerformed

    private void btncaribukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncaribukuActionPerformed
    
    }//GEN-LAST:event_btncaribukuActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
  dialog();
        peminjamanDialog.pack();
        peminjamanDialog.setAlwaysOnTop(true);
        peminjamanDialog.setVisible(true);
                // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void tbl_bukuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_bukuMouseClicked

        int baris = tbl_buku.getSelectedRow();
        kode_buku.setText(tbl_buku.getModel().getValueAt(baris,1).toString());
        judul.setText(tbl_buku.getModel().getValueAt(baris,2).toString());
       penerbit.setText(tbl_buku.getModel().getValueAt(baris,3).toString());
         jumlah.setText(tbl_buku.getModel().getValueAt(baris,4).toString());
       kurangbuku();  
       peminjamanDialog.setVisible(false);        
     
    }//GEN-LAST:event_tbl_bukuMouseClicked

    private void txtcari3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcari3KeyReleased
   // TODO add your handling code here:
    }//GEN-LAST:event_txtcari3KeyReleased

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
reload();        // TODO add your handling code here:
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jml_pinjamKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jml_pinjamKeyReleased
 kurangbuku();         // TODO add your handling code here:
    }//GEN-LAST:event_jml_pinjamKeyReleased

    private void jumlahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jumlahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jumlahActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(datapeminjaman.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(datapeminjaman.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(datapeminjaman.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(datapeminjaman.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new datapeminjaman().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea alamat;
    private javax.swing.JButton btedit;
    private javax.swing.JButton bthapus;
    private javax.swing.JButton btkembali;
    private javax.swing.JButton btncaribuku;
    private javax.swing.JButton btsimpan;
    private javax.swing.JTextField cari;
    private javax.swing.JButton cari_peminjaman;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JTextField jml_pinjam;
    private javax.swing.JTextField judul;
    private javax.swing.JTextField jumlah;
    private javax.swing.JTextField kode_buku;
    private javax.swing.JTextField kode_pinjam;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField nis;
    private javax.swing.JTextField no_anggota;
    private javax.swing.JDialog peminjamanDialog;
    private javax.swing.JTextField penerbit;
    private com.toedter.calendar.JDateChooser tanggalharuskembali;
    private javax.swing.JTable tb_buku;
    private javax.swing.JTable tbl;
    private javax.swing.JTable tbl_buku;
    private javax.swing.JTextField txtcari3;
    // End of variables declaration//GEN-END:variables
private void edit(){
kode_pinjam.setEnabled(true);
no_anggota.setEnabled(true);
nama.setEnabled(true);
kode_buku.setEnabled(true);
judul.setEnabled(true);
penerbit.setEnabled(true);
//tgl_pinjam.setEnabled(true);
nis.setEnabled(true);
alamat.setEnabled(true);
tanggalharuskembali.setEnabled(true);
}
private void kosong(){
kode_pinjam.setEnabled(false);
no_anggota.setEnabled(false);
nama.setEnabled(false);
kode_buku.setEnabled(false);
judul.setEnabled(false);
penerbit.setEnabled(false);
//tgl_pinjam.setEnabled(false);
nis.setEnabled(false);
alamat.setEnabled(false);
tanggalharuskembali.setEnabled(false);
}
   
 private void dialog() {
        // membuat tampilan model tabel
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("no");
        model.addColumn("kode buku");
        model.addColumn("judul");
        model.addColumn("penerbit");
        model.addColumn("jumlah");
  
        
        
         try {
            int no = 1;
            String sql = "select * from databuku where judul like '%"+cari.getText()+"%' OR kdbuku like '%"+cari.getText()+"%' OR penerbit like '%"+cari.getText()+"%'";
            java.sql.Connection conn = (java.sql.Connection) config.configDB();
            java.sql.Statement stm = conn.createStatement();
            java.sql.ResultSet res = stm.executeQuery(sql);
            while (res.next()) {
                model.addRow(new Object[]{no++, res.getString(1), res.getString(2),res.getString(4), res.getString(6)});
            }
            tbl_buku.setModel(model);
        } catch (Exception e) {
        
        }    
   
   }

private void reload(){
new datapeminjaman().setVisible(true);
this.dispose();
}


private void kurangbuku(){
int jml, qty;
   int kembali = 0;

        qty = Integer.valueOf(jumlah.getText());
        jml = Integer.valueOf(jml_pinjam.getText());
        kembali = qty-jml;
        jumlah.setText(String.valueOf(kembali));  

}
}
