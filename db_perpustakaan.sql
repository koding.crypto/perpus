-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2019 at 01:22 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `databuku`
--

CREATE TABLE IF NOT EXISTS `databuku` (
  `kdbuku` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `pengarang` varchar(50) NOT NULL,
  `penerbit` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `no` varchar(50) NOT NULL,
  PRIMARY KEY (`kdbuku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `databuku`
--

INSERT INTO `databuku` (`kdbuku`, `judul`, `pengarang`, `penerbit`, `kategori`, `jumlah`, `no`) VALUES
('BKU0001', 'Bahasa Indonesia', 'Sutanto', 'Gramedia', 'Buku Pelajaran', 9, 'A1'),
('BKU0002', 'Matematika', 'Andana', 'Trans Medis', 'Buku Pelajaran', 9, 'A2'),
('BKU0003', 'Bahasa Inggris', 'William', 'Triamedia', 'Buku Pelajaran', 10, 'A3'),
('BKU0004', 'Sejarah', 'Amenta', 'Multikarya', 'Buku Pelajaran', 9, 'A4'),
('BKU0005', 'Laskar Pelangi', 'Jaja Subagya', 'Lamanta', 'Novel', 10, 'A5');

-- --------------------------------------------------------

--
-- Table structure for table `data_anggota`
--

CREATE TABLE IF NOT EXISTS `data_anggota` (
  `no_anggota` varchar(30) NOT NULL,
  `nis` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `notlp` varchar(20) NOT NULL,
  PRIMARY KEY (`no_anggota`),
  UNIQUE KEY `nis` (`nis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_anggota`
--

INSERT INTO `data_anggota` (`no_anggota`, `nis`, `nama`, `jenis`, `alamat`, `notlp`) VALUES
('AGT0001', '2015435018597', 'Yeni Mariyana', 'Perempuan', 'Jakarta', '089636103371'),
('AGT0002', '201543501880', 'Nadya', 'Perempuan', 'Bekasi', '089636103371'),
('AGT0003', '201543502004', 'Tri Widya', 'Perempuan', 'bojong', '0897161616'),
('AGT0004', '201543501850', 'Yeni Mariyana', 'Perempuan', 'Jakarta', '0821343434'),
('AGT0005', '201543501999', 'Ahmad Andi', 'Laki Laki', 'Jakarta', '08201918121');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` varchar(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
('1', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE IF NOT EXISTS `peminjaman` (
  `kode_pinjam` varchar(25) NOT NULL,
  `no_anggota` varchar(20) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kdbuku` varchar(20) NOT NULL,
  `judul` varchar(30) NOT NULL,
  `penerbit` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal_pinjam` varchar(25) NOT NULL,
  `tanggalharuskembali` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`kode_pinjam`, `no_anggota`, `nis`, `nama`, `alamat`, `kdbuku`, `judul`, `penerbit`, `jumlah`, `tanggal_pinjam`, `tanggalharuskembali`) VALUES
('TR0001', 'AGT0001', '2015435018597', 'Yeni Mariyana', 'Jakarta', 'BKU0001', 'Bahasa Indonesia', 'Gramedia', 1, '2019-07-22', '2019-07-29'),
('TR0002', 'AGT0002', '201543501880', 'Nadya', 'Bekasi', 'BKU0002', 'Matematika', 'Trans Medis', 1, '2019-07-22', '2019-07-29'),
('TR0003', 'AGT0003', '201543502004', 'Tri Widya', 'bojong', 'BKU0004', 'Sejarah', 'Multikarya', 1, '2019-07-22', '2019-07-29');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE IF NOT EXISTS `pengembalian` (
  `kode_kembali` varchar(25) NOT NULL,
  `kode_pinjam` varchar(50) NOT NULL,
  `no_anggota` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_pinjam` varchar(20) NOT NULL,
  `tanggal_kembali` varchar(20) NOT NULL,
  `kdbuku` varchar(20) NOT NULL,
  `judul` varchar(20) NOT NULL,
  `penerbit` varchar(20) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterlambatan` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  PRIMARY KEY (`kode_kembali`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengembalian`
--

INSERT INTO `pengembalian` (`kode_kembali`, `kode_pinjam`, `no_anggota`, `nama`, `tanggal_pinjam`, `tanggal_kembali`, `kdbuku`, `judul`, `penerbit`, `jumlah`, `keterlambatan`, `denda`) VALUES
('KEM0001', 'TR0004', 'AGT0004', 'Yeni Mariyana', '2019-07-22', '2019-07-29', 'BKU0001', 'Bahasa Indonesia', 'Gramedia', 1, -7, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
